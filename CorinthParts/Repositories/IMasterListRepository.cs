﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CorinthParts.Models;

namespace CorinthParts.Repositories
{
    public interface IMasterListRepository
    {
        List<MasterList> GetMasterList(string custCode, bool isAdmin = false);
    }
}