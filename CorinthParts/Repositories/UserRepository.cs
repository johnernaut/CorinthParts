﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CorinthParts.Models;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CorinthParts.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public User FindByCustomer(string custNum)
        {
            return this._db.Query<User>("SELECT * FROM COR_Parts_UserInfo WHERE Customer = @Customer",
                new { Customer = custNum}).FirstOrDefault();
        }

        public User LoginUser(string cust, string password)
        {
            return this._db.Query<User>("SELECT * FROM COR_Parts_UserInfo WHERE Customer = @Customer AND CustomerPassword = @Password",
                new { Customer = cust, Password = password }).FirstOrDefault();
        }

        public User LoginByGuid(string guid)
        {
            return this._db.Query<User>("SELECT * FROM COR_Parts_UserInfo WHERE OverrideGuid = @Guid",
                new { Guid = guid }).FirstOrDefault();
        }

        //public bool UserCanViewPO(string poId, string supplier, bool isAdmin = false)
        //{
        //    if (isAdmin)
        //        return true;

        //    var detail = this._db.Query<OrderHeader>("SELECT * FROM VENDOR_OrderHeader WHERE PurchaseOrder = @PurchaseOrder", new { PurchaseOrder = poId }).FirstOrDefault();
        //    return detail.Supplier == supplier;
        //}
    }
}