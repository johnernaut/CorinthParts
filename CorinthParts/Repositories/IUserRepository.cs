﻿using CorinthParts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorinthParts.Repositories
{
    public interface IUserRepository
    {
        //List<User> GetAll();
        User LoginUser(string supplier, string password);
        User LoginByGuid(string guid);
        User FindByCustomer(string custNum);
        //bool UserCanViewPO(string poId, string supplier, bool isAdmin = false);
    }
}