﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CorinthParts.Models;
using Dapper;

namespace CorinthParts.Repositories
{
    public class MasterListRepository : BaseRepository, IMasterListRepository
    {
        public List<MasterList> GetMasterList(string custCode, bool isAdmin = false)
        {
            if (isAdmin)
                return this._db.Query<MasterList>("SELECT * FROM COR_Parts_WebListing").ToList();

            return this._db.Query<MasterList>("SELECT * FROM COR_Parts_WebListing WHERE Customer = @Customer", new { Customer = custCode }).ToList();
        }
    }
}