﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CorinthParts.Repositories
{
    public class BaseRepository : IDisposable
    {
        protected DbConnection _db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (this._db.State == ConnectionState.Open)
                {
                    this._db.Close();
                }
            }

            // Free unmanaged objects.
            _disposed = true;
        }
    }
}