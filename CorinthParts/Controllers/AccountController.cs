﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CorinthParts.Models;
using CorinthParts.ViewModels;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using CorinthParts.Repositories;

namespace CorinthParts.Controllers
{
    public class AccountController : Controller
    {
        private IUserRepository _userRepo = new UserRepository();

        IAuthenticationManager Authentication
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        public ActionResult Login(string returnUrl)
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("index", "admin");
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ActionResult LoginByGuid(string guid)
        {
            var user = _userRepo.LoginByGuid(guid);

            if (user != null)
            {
                var userRole = user.Role == null ? "Customer" : user.Role;

                var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, user.Customer),
                            new Claim("CustomerName", user.CustomerName),
                            new Claim(ClaimTypes.NameIdentifier, user.Customer)
                        },
                    DefaultAuthenticationTypes.ApplicationCookie,
                    ClaimTypes.Name, ClaimTypes.Role);
                identity.AddClaim(new Claim(ClaimTypes.Role, userRole));

                Authentication.SignIn(new AuthenticationProperties
                {
                    IsPersistent = false
                }, identity);

                return RedirectToAction("index", "admin");
            }

            TempData["UserMessage"] = "There was a problem logging in with that GUID.  Try entering your customer name and password in the login form, or contact an administrator.";
            return RedirectToAction("index", "admin");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = _userRepo.LoginUser(model.Customer, model.Password);

                if (user != null)
                {
                    var userRole = user.Role == null ? "Customer" : user.Role;

                    var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, model.Customer),
                            new Claim(ClaimTypes.NameIdentifier, model.Customer)
                        },
                        DefaultAuthenticationTypes.ApplicationCookie,
                        ClaimTypes.Name, ClaimTypes.Role);
                    identity.AddClaim(new Claim(ClaimTypes.Role, userRole));

                    Authentication.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = model.RememberMe
                    }, identity);

                    return RedirectToAction("index", "admin");
                }

                ModelState.AddModelError("", "There was a problem with your customer code or password.  Please contact an administrator.");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("login");
        }
    }
}