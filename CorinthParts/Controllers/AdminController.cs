﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CorinthParts.Repositories;
using CorinthParts.ViewModels;
using CorinthParts.Models;

namespace CorinthParts.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private IMasterListRepository _masterRepo = new MasterListRepository();
        private IUserRepository _userRepo = new UserRepository();
        public ActionResult Index(string po)
        {
            var model = new MasterListViewModel();

            //if (po != null)
            //{
            //    model.OrderHeaders = _headerRepo.FindByPO(po);
            //    return View(model);
            //}

            model.MasterLists = _masterRepo.GetMasterList(User.Identity.Name, User.IsInRole("Admin"));
            ViewBag.CustomerName = _userRepo.FindByCustomer(User.Identity.Name).CustomerName;

            return View(model);
        }
    }
}