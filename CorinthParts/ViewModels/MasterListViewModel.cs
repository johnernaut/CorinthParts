﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CorinthParts.Models;

namespace CorinthParts.ViewModels
{
    public class MasterListViewModel
    {
        public List<MasterList> MasterLists { get; set; }
    }
}