﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorinthParts.Models
{
    public class MasterList
    {
        public string Customer { get; set; }
        public string CustomerPo { get; set; }
        public string ShipStyle { get; set; }
        public string Serial { get; set; }
        public string PartStatus { get; set; }
        public string StockStyle { get; set; }
        public string StockDescription { get; set; }
        public string TrackingNumber { get; set; }
        public string Invoice { get; set; }
        public string ShipWithPo { get; set; }
        public DateTime CapturedOn { get; set; }
        public DateTime WoCompletedOn { get; set; }
        public string Name { get; set; }
        public string ShipAddress1 { get; set; }
        public string ShipAddress2 { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipPostalCode { get; set; }
    }
}