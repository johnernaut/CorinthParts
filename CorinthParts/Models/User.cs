﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorinthParts.Models
{
    public class User
    {
        public string Customer { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPassword { get; set; }
        public string Role { get; set; }
        public string OverrideGuid { get; set; }
    }
}