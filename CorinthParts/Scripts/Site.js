﻿$(document).ready(function () {
    $(".search").keyup(function () {
        var searchTerm = $(".search").val();
        var listItem = $('.results tbody').children('tr');
        var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

        $.extend($.expr[':'], {
            'containsi': function (elem, i, match, array) {
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
        });

        $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'false');
        });

        $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
            $(this).attr('visible', 'true');
        });

        var jobCount = $('.results tbody tr[visible="true"]').length;
        $('.counter').text(jobCount + ' item(s)');

        if (jobCount == '0') { $('.no-result').show(); }
        else { $('.no-result').hide(); }

        if (searchTerm === "") {
            $('.results tbody tr[visible="false"]').each(function (index) {
                $(this).attr('visible', 'true');
            });
            $('.no-result').hide()
        }
    });

    $('#update-orderdetail').submit(function (evt) {
        var data = form2js('update-orderdetail');
        console.log(data);
        evt.preventDefault();

        $.ajax({
            type: 'POST',
            url: $(this).attr("action"),
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function () {
                $('.alert').addClass('alert-success').removeClass('hidden').append('<strong>Succes!</strong> You successfully saved these order details.');
            },
            error: function () {
                $('.alert').addClass('alert-danger').removeClass('hidden').append('<strong>Error!</strong> Could not save order details.  Please contact an administrator.');
            }
        });
    });
});